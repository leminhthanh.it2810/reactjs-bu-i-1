import React, { Component } from "react";

export default class Item extends Component {
  render() {
    return (
      <div className="row gx-lg-5">
        <div className="card bg-light col-sm-4 mb-5">
          <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
            <div className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
              <i class="fa fa-code"></i>
            </div>
            <h2 className="fs-4 fw-bold">Fresh new layout</h2>
            <p className="mb-0">
              With Bootstrap 5, we've created a fresh new layout for this
              template!
            </p>
          </div>
        </div>
        
        <div className="card bg-light col-sm-4 mb-5">
          <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
            <div className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
              <i class="fa fa-code"></i>
            </div>
            <h2 className="fs-4 fw-bold">Fresh new layout</h2>
            <p className="mb-0">
              With Bootstrap 5, we've created a fresh new layout for this
              template!
            </p>
          </div>
        </div>

        <div className="card bg-light col-sm-4 mb-5">
          <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
            <div className="feature bg-primary text-white  mb-4 mt-n4">
              <i class="fa fa-code"></i>
            </div>
            <h2 className="fs-4 fw-bold">Fresh new layout</h2>
            <p className="mb-0">
              With Bootstrap 5, we've created a fresh new layout for this
              template!
            </p>
          </div>
        </div>
        <div className="card bg-light col-sm-4 mb-5">
          <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
            <div className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
              <i class="fa fa-code"></i>
            </div>
            <h2 className="fs-4 fw-bold">Fresh new layout</h2>
            <p className="mb-0">
              With Bootstrap 5, we've created a fresh new layout for this
              template!
            </p>
          </div>
        </div>
        <div className="card bg-light col-sm-4 mb-5">
          <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
            <div className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
              <i class="fa fa-code"></i>
            </div>
            <h2 className="fs-4 fw-bold">Fresh new layout</h2>
            <p className="mb-0">
              With Bootstrap 5, we've created a fresh new layout for this
              template!
            </p>
          </div>
        </div>
        <div className="card bg-light col-sm-4 mb-5">
          <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
            <div className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
              <i class="fa fa-code"></i>
            </div>
            <h2 className="fs-4 fw-bold">Fresh new layout</h2>
            <p className="mb-0">
              With Bootstrap 5, we've created a fresh new layout for this
              template!
            </p>
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import Banner from "./Banner";
import Item from "./Item";

export default class Body extends Component {
  render() {
    return (
      <div className="py-5">
        <Banner />
        <div className="pt-4">
          <div className="container px-lg-5">
            <div className="row ">
              <Item />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
